<?php

use Illuminate\Database\Seeder;

class fieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('fields')->insert([
      	[
       		'venue_id' => 1,
       		'pitch_id' => 1,
       		'status' => 0,
      	],
      	[
       		'venue_id' => 1,
       		'pitch_id' => 2,
       		'status' => 0,
				],
				[
       		'venue_id' => 1,
       		'pitch_id' => 3,
       		'status' => 0,
      	],
      	[
       		'venue_id' => 1,
       		'pitch_id' => 4,
       		'status' => 0,
				]
        	
      ]);
    }
}
