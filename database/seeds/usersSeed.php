<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class usersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
           'name' => 'ly',
           'email' => 'lytest@gmail.com',
            
					'password' => bcrypt('secret'),
            // $table->rememberToken();
            // $table->timestamps();
        	],
        	[
           'name' => str_random(10),
           'email' => str_random(10).'@gmail.com',
            
					'password' => bcrypt('secret'),
            // $table->rememberToken();
            // $table->timestamps();
				]
        	
        ]);
    }
}
