<?php

use Illuminate\Database\Seeder;

class venueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('venues')->insert([
        	[
        		'name' =>"4D football Club",
           		'image' => "storage/images/4d_football_club.jpg",
           		'address' => 'Phum Thnoat, Sangkat Steung Mean Chey, Khan Mean Chey, Phnom Penh, Phnom Penh 12352',
           		'description'=>'open from 6AM - 10PM',
           		'phone'=>'010 472 222'
        	],
        	[
           		'name' =>"Punhea Hok Football Pitch",
           		'image' => "storage/images/punhea_hok_football.JPG",
           		'address' => 'Phnom Penh',
           		'description'=>'open from 6AM - 10PM',
           		'phone'=>'098 831 177'
			],
			[
           		'name' =>"Lucky Sport",
           		'image' => "storage/images/lucky_sport.jpg",
           		'address' => '#152, St. 69BT, Sangkat Tumup Teuk, Khan Mean Chey, Phnom Penh 12351, Phnom Penh',
           		'description'=>'open from 6AM - 10PM',
           		'phone'=>'023 695 8262'
        	],
        	[
           		'name' =>"Light Field Football Field",
           		'image' => "storage/images/light_field.jpg",
           		'address' => '26BT, Phnom Penh',
           		'description'=>'open from 6AM - 10PM',
           		'phone'=>'096 389 4657'
			],
			[
           		'name' =>"KB All Sport Club",
           		'image' => "storage/images/kb_all_sport_club.jpg",
           		'address' => 'វិថី សម្តេចហ៊ុនសែន, Phnom Penh 12301',
           		'description'=>'open from 7AM - 11PM',
           		'phone'=>'012 798 168'
        	],
        	[
           		'name' =>"Akira Sport Club",
           		'image' => "storage/images/akira_sport_club.jpg",
           		'address' => '#42, St. 656, Sangkat Toek Laak III, Khan Tuol Kork, Phnom Penh, Phnom Penh 12158',
           		'description'=>'open from 6AM - 11PM',
           		'phone'=>'099 767 677'
			]
        	
        ]);
    }
}
