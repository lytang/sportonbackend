<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	// $this->call(usersSeed::class);
    	$this->call(pitchTableSeeder::class);
    	$this->call(venueTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        $this->call(fieldTableSeeder::class);
    }
}
