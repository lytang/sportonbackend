<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class pitchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('picthes')->insert([
        	[
           		'image' => "storage/images/5v5.jpg",
           		'size' => '5v5',
        	],
        	[
           		'image' => "storage/images/7v7.jpg",
           		'size' => '7v7',
			],
			[
           		'image' => "storage/images/9v9.jpg",
           		'size' => '9v9',
        	],
        	[
           		'image' => "storage/images/11v11.jpg",
           		'size' => '11v11',
			]
        	
        ]);
    }
}
