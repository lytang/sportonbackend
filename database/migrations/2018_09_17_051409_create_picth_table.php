<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('picthes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('size');
            // $table->unsignedInteger('venue_id');
            // $table->foreign('venue_id')->references('id')->on('venue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picthes');
    }
}
