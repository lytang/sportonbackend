<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservates', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->unsignedInteger('team_a');
            $table->unsignedInteger('team_b');
            $table->unsignedInteger('match_id');
            $table->boolean('need_opponent');
            $table->foreign('match_id')->references('id')->on('sport_matches');
            $table->foreign('team_a')->references('id')->on('teams');
            $table->foreign('team_b')->references('id')->on('teams');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservates');
    }
}
