<?php

use Illuminate\Http\Request;
// use App\
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', function (Request $request) {
    
    if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
        // Authentication passed...
        $user = auth()->user();
        $user->api_token = str_random(60);
        $user->save();
        return $user;
    }
    
    return response()->json([
        'error' => 'Unauthenticated user',
        'code' => 401,
    ], 401);
});
Route::middleware('auth:api')->post('logout', function (Request $request) {
    
    if (auth()->user()) {
        $user = auth()->user();
        $user->api_token = null; // clear api token
        $user->save();

        return response()->json([
            'message' => 'Thank you for using our application',
        ]);
    }
    
    return response()->json([
        'error' => 'Unable to logout user',
        'code' => 401,
    ], 401);
});
// Route::middleware('auth:api')->post('/user/register','RegisterController@index');
// Route::middleware('auth:api')->post('/user/login','LoginController@index');
//Match Api
Route::get('matches','Api\MatchController@index');
Route::post('/match','Api\MatchController@store');
Route::post('/match/{id}','Api\MatchController@edit');
Route::post('/match/show/{id}','Api\MatchController@show');
Route::post('/match/updateOpponent/{id}','Api\MatchController@update');
// Route::post('/match/find','MatchController@matchup');
// //Friend Api
// Route::middleware('friend:api')->get('/friends', 'FriendController@index');
// Route::middleware('friend:api')->post('/friend','FriendController@addFriend');
// Route::middleware('friend:api')->get('/friend/{id}','FriendController@showFriend');

// //Team Api
// Route::middleware('team:api')->get('/teams', 'TeamController@index');
// Route::middleware('team:api')->post('/team','TeamController@createTeam');
// Route::middleware('team:api')->get('/team/{id}','TeamController@showTeam');
// Route::middleware('team:api')->post('/team/member','TeamController@addTeamMember');
// Route::middleware('team:api')->post('/team/{id}','TeamController@updateTeam');
// Route::middleware('team:api')->post('/team/member/{id}','TeamController@removeMember');

// //Venue_Field Api
Route::get('venues', 'Api\FieldController@index');
// Route::middleware('field:api')->get('/venues','Api\FieldController@index');
// Route::middleware('field:api')->get('/venue/{id}','Api\FieldController@show');