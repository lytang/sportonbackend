<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\
class SportMatches extends Model
{
	// protected $match = "App\Match";
	protected $table = 'sport_matches';
	protected $reservation = "App\Reservation";
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'date','referee','host','opponent','start_time','end_time','field_id',
    ];
    public function findMatch(){
    	$need_opponet_matches = sport_matches::select('sport_match.*','reservation.*')
    								  ->join('reservation','reservation.match_id','=','match.id')
    								  ->where('reservation.needOpponent',true)
    								  ->get();
    	return $need_opponet_matches;
    }

    public function upcommingMatch(){
    	$matches = $reservation->selectRaw(DB::Raw('reservates.*', 'sport_matches.*','teams.*'))
    						->join('sport_matches','reservates.match_id','=','sport_matches.id')
    					       ->join('teams', function ($join) {
            					$join->on('reservates.team_a', '=', 'team.id')->orOn('reservates.team_b','=','team.id');
        						})
    					       ->select('*')
    					       ->orderBy('sport_matches.date', 'desc')
    					      ->limit(15)
                			  ->get();
        return $matches;
    }

    public function cancelMatch($date, $team){
    	//find id of match and id of reservation to cancel/remove team from reservation
    }

    public function createMatch($data){

    }

    public function updateMatch($id,$data){
    	
    }
}
