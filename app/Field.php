<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{	protected $table = 'fields';
	protected $fillable = ['venue_id', 'pitch_id'];
    //
    public function venue()
    {
        return $this->belongsTo('App\Models\Venue');
    }

    public function pitch()
    {
        return $this->hasOne('App\Models\Pitch');
    }

}
