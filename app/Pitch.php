<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pitch extends Model
{
	protected $table = 'pitches';
	public function fields(){
		return this->belongsToMany('App\Models\Field');
	}
    //
}
