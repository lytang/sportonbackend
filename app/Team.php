<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{	
	protected $team = "App\Team";
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'logo', 'user_id',
    ];
	protected $table = 'teams';
	public function users(){
		return this->belongsToMany('App\Models\User');
	}
    //
    public function addMembers($data){
    	$team->insert($data);
    }

    public function deleteMember($team_id, $user_id){
    	$delete_member = $team->where('team_id',$team_id)
    						  ->where('user_id',$user_id)
    						  ->delete();
    }

    public function listAllMembers($team_id){
    	$members = $team->join('user','user.id','=','team.user_id')
    					->where('team.id',$team_id)
    					->get();
    	return $members;
    }

    public function listAllGroupOfUser($user_id){
    	$teams = $team->where('user_id',$user_id)
    				  ->get();
    	return $teams;
    }
}
