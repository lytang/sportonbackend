<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SportMatches;
use App\Reservates;
use DB;
use Input;
// use Illuminate\Support\Facades\Input;
class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $upcomming_matches = SportMatch::upcommingMatch();
        // // 
        // return $upcommingMatch;
        $data = Reservates::selectRaw(DB::Raw('reservates.*', 'sport_matches.*','teams.*'))
                ->join('sport_matches','reservates.match_id','=','sport_matches.id')
                   ->join('teams', function ($join) {
                    $join->on('reservates.team_a', '=', 'teams.id')->orOn('reservates.team_b','=','teams.id');
                    })
                   // ->select('*')
                   ->orderBy('sport_matches.date', 'desc')
                  ->limit(15)
                  ->get();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
      $match_title = Input::get('title');
      $match_date = Input::get('date');
      $match_refrecee = Input::get('referee');
      $start_time = Input::get('start_time');
      $end_time = Input::get('end_time');
      $field_id = Input::get('field_id');
      $team_a = Input::get('home_team');
      $team_b = Input::get('away_team');
      $need_opponent = Input::get('need_opponent');
      $booking_status = Input::get('booking_status');
      // $created_at = new Date()
      $sport_match = new SportMatches;
      $sport_match->title = $match_title;
      $sport_match->refrecee = $match_refrecee;
      $sport_match->date = $match_date;
      $sport_match->start_time = $start_time;
      $sport_match->end_time = $end_time;
      $sport_match->field_id = $field_id;
      if($sport_match->save()){
        $reservation = new Reservates;
        $reservation->status = $booking_status;
        $reservation->team_a = $team_a;
        $reservation->team_b = $team_b;
        $reservation->need_opponent = $need_opponent;
        $reservation->match_id = $sport_match->id;
        if($reservation->save()){
          return Response::json(['status' => 1, 'msg' => "Successed", 'messeage' => $message]);
          }else{
            return Response::json(['status' => 0, 'msg' => "Error"]);
        }
      }else{
        return Response::json(['status' => 0, 'msg' => "Error"]);
      }
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Reservates::selectRaw(DB::Raw('reservates.*', 'sport_matches.*','teams.*'))
                 ->join('sport_matches','reservates.match_id','=','sport_matches.id')
                 ->join('teams', function ($join) {
                  $join->on('reservates.team_a', '=', 'teams.id')->orOn('reservates.team_b','=','teams.id');
                  })
                 ->where('sport_matches.id',$id)
                 // ->select('*')
                 // ->orderBy('sport_matches.date', 'desc')
                // ->limit(15)
                ->get();
      return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $match_title = Input::get('title');
      $match_date = Input::get('date');
      $match_refrecee = Input::get('referee');
      $start_time = Input::get('start_time');
      $end_time = Input::get('end_time');
      $field_id = Input::get('field_id');
      $team_a = Input::get('home_team');
      $team_b = Input::get('away_team');
      $need_opponent = Input::get('need_opponent');
      $booking_status = Input::get('booking_status');

      $match = SportMatches::find($id);

      $match->start_time = $start_time;
      $match->end_time = $end_time;
      $match->refrecee = $match_refrecee;
      $match->date = $match_date;
      // $match->title = $
      if($match->save()){
        return Response::json(['status' => 1, 'msg' => "Successed", 'messeage' => $message]);
      }else{
        return Response::json(['status' => 0, 'msg' => "Error"]);
      }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $match_title = Input::get('title');
      $match_date = Input::get('date');
      $match_refrecee = Input::get('referee');
      $start_time = Input::get('start_time');
      $end_time = Input::get('end_time');
      $field_id = Input::get('field_id');
      $team_a = Input::get('home_team');
      $team_b = Input::get('away_team');
      $need_opponent = Input::get('need_opponent');
      $booking_status = Input::get('booking_status');

      $reservation = Reservates::where('match_id',$id)->first();

      $reservation->team_a = $team_a;
      $reservation->team_b = $team_b;
      $reservation->need_opponent = $need_opponent;
      $reservation->status = $booking_status;
      if($reservation->save()){
        return Response::json(['status' => 1, 'msg' => "Successed", 'messeage' => $message]);
      }else{
        return Response::json(['status' => 0, 'msg' => "Error"]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
