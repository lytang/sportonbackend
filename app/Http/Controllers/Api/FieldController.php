<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Http\Controllers\Api;
use App\Field;
use DB;
use Input;
class FieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Field::all()
        $venue_field = Field::selectRaw(DB::Raw('fields.id, venue.name, venue.image, venue.address, venue.description, venue.phone'))
                        ->join('venue', 'fields.venue_id','=','venue.id')
                        ->get();
        return $venue_field;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $fieldId = Input::get('field_id');
        $venue_field = DB::table('field')
                        ->join('venue', 'field.venue_id','=','venue.id')
                        ->where('field.id',$id)
                        ->get();
        return $venue_field;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
