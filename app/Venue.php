<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
	protected $table = 'venues';
	public function fields(){
		return this->belongsToMany('App\Models\Field');
	}
    //
}
