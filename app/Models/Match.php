<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\
class SportMatch extends Model
{
	// protected $match = "App\Match";
	protected $reservation = "App\Reservation";
	protected $table = 'sport_match';
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'date','referee','host','opponent','start_time','end_time','field_id',
    ];
    public function findMatch(){
    	$need_opponet_matches = sport_match::select('sport_match.*','reservation.*')
    								  ->join('reservation','reservation.match_id','=','match.id')
    								  ->where('reservation.needOpponent',true)
    								  ->get();
    	return $need_opponet_matches;
    }

    public function upcommingMatch(){
    	$matches = $reservation->join('match','reservation.match_id','=','match.id')
    					       ->join('team', function ($join) {
            					$join->on('reservation.team_a', '=', 'team.id')->orOn('reservation.team_b','=','team.id');
        						})
    					       ->select('*')
    					       ->orderBy('match.date', 'desc')
    					      ->limit(15)
                			  ->get();
        return $matches;
    }

    public function cancelMatch($date, $team){
    	//find id of match and id of reservation to cancel/remove team from reservation
    }

    public function createMatch($data){

    }

    public function updateMatch($id,$data){
    	
    }
}
